# Description
This is a script using nodejs and puppeteer to toggle a fritzbox Wlan connection

# Dependencies
- NodeJS

# Install
1. Clone the project
2. Run „npm install“
3. Add the login credentials in the credentials.json file

# Usage
Run „node index.js“ 

## Windows
Use togglewlan.bat

## Mac
User togglewlan.sh
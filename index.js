var credentials = require('./credentials.json');
const puppeteer = require('puppeteer');



(async () => {
  const browser = await puppeteer.launch({slowMo: 500});

  // DEV ONLY
  // const browser = await puppeteer.launch({headless: false, slowMo: 500});

  const page = await browser.newPage();
  await page.goto(credentials.url);
  await page.$eval('#uiPass', (el, value) => el.value = value, credentials.password);
  await page.click('#submitLoginBtn');
  await page.click('#blueBarLogo');
  await page.click('#wlan');
  await page.click('#wSet');
  await page.click('#uiApActive');
  await page.click('#uiApActiveScnd');
  await page.click('button[name="apply"]')

  await browser.close();
})();